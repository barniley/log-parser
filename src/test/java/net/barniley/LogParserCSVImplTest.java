package net.barniley;

import net.barniley.domain.logfile.LogEntry;
import net.barniley.domain.logfile.LogFile;
import net.barniley.domain.logfile.LogParserCSVImpl;
import net.barniley.utils.Utils;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

public class LogParserCSVImplTest extends Assert {
    @Test
    public void parseOneDayTest() {
        String logContents = "1455812018,user2,http://ru.wikipedia.org,100";
        ByteArrayInputStream in = new ByteArrayInputStream(logContents.getBytes());
        LogFile logFile = new LogParserCSVImpl(in);
        logFile.parse();
        Set<LogEntry> expectedLogEntries = new HashSet<>();
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(1455812018);
        Utils.setDateToMidnight(date);
        expectedLogEntries.add(new LogEntry(date, "user2", "http://ru.wikipedia.org", 100));
        assertEquals(expectedLogEntries, logFile.getLogEntries());
    }

    @Test
    public void parseTwoDayTest() {
        String logContents = "1460581190000,user1,http://ru.wikipedia.org,100";
        ByteArrayInputStream in = new ByteArrayInputStream(logContents.getBytes());
        LogFile logFile = new LogParserCSVImpl(in);
        logFile.parse();
        Set<LogEntry> expectedLogEntries = new HashSet<>();
        Calendar startDate = Calendar.getInstance();
        startDate.setTimeInMillis(Long.parseLong("1460581190000"));
        Calendar endDate = Calendar.getInstance();
        endDate.setTimeInMillis(Long.parseLong("1460581290000"));
        Utils.setDateToMidnight(startDate);
        Utils.setDateToMidnight(endDate);
        expectedLogEntries.add(new LogEntry(startDate, "user1", "http://ru.wikipedia.org", 10));
        expectedLogEntries.add(new LogEntry(endDate, "user1", "http://ru.wikipedia.org", 90));
        assertEquals(expectedLogEntries, logFile.getLogEntries());
    }

}
