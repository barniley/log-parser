package net.barniley;

import net.barniley.domain.logfile.LogEntry;
import net.barniley.domain.logfile.LogFile;
import net.barniley.domain.logfile.LogParserXMLImpl;
import net.barniley.utils.Utils;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

public class LogParserXMLImplTest extends Assert {
    @Test
    public void parseOneDayTest() {

        String logContents = "<logs>\n" +
                "    <log>\n" +
                "        <userId>user2</userId>\n" +
                "        <time>1455812018</time>\n" +
                "        <url>http://ru.wikipedia.org</url>\n" +
                "        <spentTime>100</spentTime>\n" +
                "    </log>\n" +
                "</logs>";
        ByteArrayInputStream in = new ByteArrayInputStream(logContents.getBytes());
        LogFile logFile = new LogParserXMLImpl(in);
        logFile.parse();
        Set<LogEntry> expectedLogEntries = new HashSet<>();
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(1455812018);
        expectedLogEntries.add(new LogEntry(Utils.setDateToMidnight(date), "user2", "http://ru.wikipedia.org", 100));
        assertEquals(expectedLogEntries, logFile.getLogEntries());
    }

    @Test
    public void parseTwoDayTest() {
        String logContents = "<logs>\n" +
                "    <log>\n" +
                "        <userId>user1</userId>\n" +
                "        <time>1460581190000</time>\n" +
                "        <url>http://ru.wikipedia.org</url>\n" +
                "        <spentTime>100</spentTime>\n" +
                "    </log>\n" +
                "</logs>";
        ByteArrayInputStream in = new ByteArrayInputStream(logContents.getBytes());
        LogFile logFile = new LogParserXMLImpl(in);
        logFile.parse();
        Set<LogEntry> expectedLogEntries = new HashSet<>();
        Calendar startDate = Calendar.getInstance();
        startDate.setTimeInMillis(Long.parseLong("1460581190000"));
        Calendar endDate = Calendar.getInstance();
        endDate.setTimeInMillis(Long.parseLong("1460581290000"));
        expectedLogEntries.add(new LogEntry(Utils.setDateToMidnight(startDate), "user1", "http://ru.wikipedia.org", 10));
        expectedLogEntries.add(new LogEntry(Utils.setDateToMidnight(endDate), "user1", "http://ru.wikipedia.org", 90));
        assertEquals(expectedLogEntries, logFile.getLogEntries());
    }
}
