package net.barniley;

import net.barniley.domain.User;
import net.barniley.domain.logfile.LogFile;
import net.barniley.domain.logfile.LogParserCSVImpl;
import net.barniley.domain.logfile.LogParserXMLImpl;
import net.barniley.domain.report.Report;
import net.barniley.domain.report.ReportCSVImpl;
import net.barniley.domain.report.ReportXMLImpl;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

public class ReportTest extends Assert {
    @Test
    public void writeCSVReportTest() {
        String logContents =
                "1460581190000,user1,http://ru.wikipedia.org,100\n" +
                        "1455812018,user2,http://ru.wikipedia.org,100\n" +
                        "1455812018,user2,http://ru.wikipedia.org,100\n" +
                        "1455812019,user10,http://hh.ru,30\n" +
                        "1455812968,user3,http://google.com,60\n" +
                        "1455812411,user10,http://hh.ru,90\n" +
                        "1455812684,user3,http://vk.com,50\n";
        ByteArrayInputStream in = new ByteArrayInputStream(logContents.getBytes());
        LogFile logFile = new LogParserCSVImpl(in);
        logFile.parse();
        Set<User> userSet = new TreeSet<>();
        for (String userId : logFile.getUserIds()) {
            userSet.add(new User(userId));
        }
        for (User user : userSet) {
            user.putAll(logFile.getLogEntriesByUser(user));
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Report report = new ReportCSVImpl(outputStream);
        report.writeReport(logFile.getDates(), userSet);
        System.out.println(outputStream.toString());
        try {
            outputStream.close();
        } catch (IOException e) {
            //NOP
        }
    }

    @Test
    public void writeXMLReportTest() {
        String logContents =
                "<logs>" +
                        "<log>" +
                        "<time>1460581190000</time>" +
                        "<userId>user1</userId>" +
                        "<url>http://ru.wikipedia.org</url>" +
                        "<spentTime>100</spentTime>" +
                        "</log>" +
                        "<log>" +
                        "<time>1455812018</time>" +
                        "<userId>user2</userId>" +
                        "<url>http://ru.wikipedia.org</url>" +
                        "<spentTime>100</spentTime>" +
                        "</log>" +
                        "<log>" +
                        "<time>1455812019</time>" +
                        "<userId>user10</userId>" +
                        "<url>http://hh.ru</url>" +
                        "<spentTime>30</spentTime>" +
                        "</log>" +
                        "<log>" +
                        "<time>1455812968</time>" +
                        "<userId>user3</userId>" +
                        "<url>http://google.com</url>" +
                        "<spentTime>60</spentTime>" +
                        "</log>" +
                        "<log>" +
                        "<time>1455812411</time>" +
                        "<userId>user10</userId>" +
                        "<url>http://hh.ru</url>" +
                        "<spentTime>90</spentTime>" +
                        "</log>" +
                        "<log>" +
                        "<time>1455812684</time>" +
                        "<userId>user3</userId>" +
                        "<url>http://vk.com</url>" +
                        "<spentTime>50</spentTime>" +
                        "</log>" +
                        "</logs>";
        ByteArrayInputStream in = new ByteArrayInputStream(logContents.getBytes());
        LogFile logFile = new LogParserXMLImpl(in);
        logFile.parse();
        Set<User> userSet = new TreeSet<>();
        for (String userId : logFile.getUserIds()) {
            userSet.add(new User(userId));
        }
        for (User user : userSet) {
            user.putAll(logFile.getLogEntriesByUser(user));
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Report report = new ReportXMLImpl(outputStream);
        report.writeReport(logFile.getDates(), userSet);
        System.out.println(outputStream.toString());
        try {
            outputStream.close();
        } catch (IOException e) {
            //NOP
        }
    }


}
