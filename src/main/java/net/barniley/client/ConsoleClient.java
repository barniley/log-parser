package net.barniley.client;

import net.barniley.domain.logfile.LogFile;
import net.barniley.domain.logfile.LogParserCSVImpl;
import net.barniley.domain.logfile.LogParserXMLImpl;
import net.barniley.domain.report.Report;
import net.barniley.domain.report.ReportCSVImpl;
import net.barniley.domain.report.ReportXMLImpl;
import net.barniley.task.FormReportTask;
import net.barniley.utils.PropertyException;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConsoleClient {
    private static String inputDirectoryPathPropName = "input.directory.path";
    private static String outputDirectoryPathPropName = "output.directory.path";
    private static String inputOutputFileExtPropName = "input.output.file.extension";
    private static Path settingsFilePath = Paths.get("settings.properties");

    private final int MAX_THREADS = 10;
    private final Set<String> scannedLogFiles;

    public ConsoleClient() {
        scannedLogFiles = new HashSet<>();
    }

    public static void main(String[] args) {
        ConsoleClient consoleClient = new ConsoleClient();
        consoleClient.start(System.in, System.out);
    }

    public void start(InputStream in, OutputStream out) {
        PrintStream ps = (out.getClass() == PrintStream.class) ? (PrintStream) out : new PrintStream(out);
        Properties properties = null;
        Path inputDirectoryPath = null;
        Path outputDirectoryPath = null;
        String logFileExtension = null;
        try {
            properties = loadProperties(settingsFilePath);
            checkProperties(properties, inputDirectoryPathPropName, outputDirectoryPathPropName, inputOutputFileExtPropName);
            inputDirectoryPath = Paths.get(properties.getProperty(inputDirectoryPathPropName));
            isDirectory(inputDirectoryPath);
            outputDirectoryPath = Paths.get(properties.getProperty(outputDirectoryPathPropName));
            isDirectory(outputDirectoryPath);
            logFileExtension = properties.getProperty(inputOutputFileExtPropName);
        } catch (Exception e) {
            ps.println(e.getMessage());
            System.exit(0);
        }
        ps.println("Working...");
        ExecutorService service = Executors.newFixedThreadPool(MAX_THREADS);
        while (true) {
            try {
                for (String fileName : monitorNewLogFiles(inputDirectoryPath, logFileExtension)) {
                    BufferedInputStream buffIS =
                            new BufferedInputStream(new FileInputStream(
                                    Paths.get(inputDirectoryPath.normalize().toString(), fileName).toFile()));
                    boolean isFileExtCSV = logFileExtension.equals(".csv");
                    LogFile logFile = isFileExtCSV ? new LogParserCSVImpl(buffIS) : new LogParserXMLImpl(buffIS);
                    BufferedOutputStream buffOS =
                            new BufferedOutputStream(new FileOutputStream(
                                    Paths.get(outputDirectoryPath.normalize().toString(), "avg_" + fileName).toFile()));
                    Report report =
                            isFileExtCSV ? new ReportCSVImpl(buffOS) : new ReportXMLImpl(buffOS);
                    service.submit(new FormReportTask(logFile, report));
                    ps.println(fileName + " is found. Writing a report to avg_" + fileName);
                }
                int sleepInMilliseconds = 2000;
                Thread.sleep(sleepInMilliseconds);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private Properties loadProperties(Path path) {
        Properties properties = new Properties();
        try {
            properties.load(new BufferedInputStream(new FileInputStream(path.toFile())));
        } catch (IOException e) {
            throw new RuntimeException("Property file " + path.getFileName() + " is not found.", e);
        }
        return properties;
    }

    private void checkProperties(Properties properties, String... keys) {
        for (String key : keys) {
            String value = properties.getProperty(key);
            if (value == null) {
                throw new PropertyException("Property " + key + " is not found in properties.");
            }
            if (value.matches("[ ]*")) {
                throw new PropertyException("Property " + key + " value is empty.");
            }
        }
    }

    private void isDirectory(Path path) throws IOException {
        if (!Files.isDirectory(path)) {
            throw new IOException("Directory " + path.normalize().toAbsolutePath() + " does not exist."); //???
        }
    }


    public Set<String> monitorNewLogFiles(Path logDirectory, final String fileExtension) {
        Set<String> newLogFilesName = new LinkedHashSet<>();
        try (DirectoryStream<Path> directoryStream
                     = Files.newDirectoryStream(logDirectory,
                new DirectoryStream.Filter<Path>() {
                    @Override
                    public boolean accept(Path entry) throws IOException {
                        String fileName = entry.getFileName().toString();
                        return fileName.endsWith(fileExtension) && !scannedLogFiles.contains(fileName);
                    }
                })) {
            for (Path path : directoryStream) {
                String fileName = path.getFileName().toString();
                scannedLogFiles.add(fileName);
                newLogFilesName.add(fileName);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return newLogFilesName;
    }
}
