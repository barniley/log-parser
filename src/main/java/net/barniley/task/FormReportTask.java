package net.barniley.task;

import net.barniley.domain.User;
import net.barniley.domain.logfile.LogFile;
import net.barniley.domain.report.Report;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

public class FormReportTask implements Runnable {
    private LogFile logFile;
    private Report report;

    public FormReportTask(LogFile logFile, Report report) {
        this.logFile = logFile;
        this.report = report;
    }

    @Override
    public void run() {
        logFile.parse();
        Set<User> userSet = new TreeSet<>();
        for (String userId : logFile.getUserIds()) {
            userSet.add(new User(userId));
        }
        for (User user : userSet) {
            user.putAll(logFile.getLogEntriesByUser(user));
        }
        report.writeReport(logFile.getDates(), userSet);

        try {
            logFile.getInputStream().close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            report.getOutputStream().close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
