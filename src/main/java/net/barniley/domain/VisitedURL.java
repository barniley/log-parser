package net.barniley.domain;

public class VisitedURL implements Comparable<VisitedURL> {
    private String url;
    private int spentTime;
    private int countVisitTimes;

    public VisitedURL(String url, int spentTime) {
        this.url = url;
        this.spentTime = spentTime;
        ++countVisitTimes;
    }

    public int getAvgTime() {
        return spentTime / countVisitTimes;
    }

    public void addTime(int amount) {
        spentTime += amount;
        ++countVisitTimes;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VisitedURL that = (VisitedURL) o;

        return url.equals(that.url);

    }

    @Override
    public int hashCode() {
        return url.hashCode();
    }

    @Override
    public String toString() {
        return "VisitedURL{" +
                "url='" + url + '\'' +
                ", spentTime=" + spentTime +
                '}';
    }

    @Override
    public int compareTo(VisitedURL url) {
        return this.url.compareTo(url.getUrl());
    }
}
