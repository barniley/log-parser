package net.barniley.domain.report;

import net.barniley.domain.User;
import net.barniley.domain.VisitedURL;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Map;
import java.util.Set;

import static net.barniley.utils.Utils.getMonth;

public class ReportXMLImpl extends Report {

    public ReportXMLImpl(OutputStream outputStream) {
        super(outputStream);
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    @Override
    public void writeReport(Set<Calendar> dates, Set<User> users) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document report = documentBuilder.newDocument();
            Element rootElement = report.createElement("report");
            rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            rootElement.setAttribute("xsi:noNamespaceSchemaLocation", "report.xsd");
            report.appendChild(rootElement);
            for (Calendar date : dates) {
                String dateHeader =
                        date.get(Calendar.DATE)
                                + "-" + getMonth(date.get(Calendar.MONTH))
                                + "-" + Integer.toString(date.get(Calendar.YEAR));
                Element dateElement = report.createElement("date");
                dateElement.setAttribute("date", dateHeader);
                for (User user : users) {
                    Element userElement = report.createElement("user");
                    userElement.setAttribute("id", user.getUserId());
                    Map<Calendar, Set<VisitedURL>> dateVisitedURLMap = user.getVisitedURLByDate();
                    if (dateVisitedURLMap.containsKey(date)) {
                        for (VisitedURL url : dateVisitedURLMap.get(date)) {
                            Element urlElement = report.createElement("url");
                            urlElement.setAttribute("avgTime", Integer.toString(url.getAvgTime()));
                            urlElement.appendChild(report.createTextNode(url.getUrl()));
                            userElement.appendChild(urlElement);
                            dateElement.appendChild(userElement);
                        }
                    }
                }
                rootElement.appendChild(dateElement);
            }
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            DOMSource domSource = new DOMSource(report);
            transformer.transform(domSource, new StreamResult(outputStream));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
