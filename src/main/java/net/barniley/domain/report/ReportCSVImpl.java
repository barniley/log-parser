package net.barniley.domain.report;

import net.barniley.domain.User;
import net.barniley.domain.VisitedURL;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Map;
import java.util.Set;

import static net.barniley.utils.Utils.getMonth;

public class ReportCSVImpl extends Report {


    public ReportCSVImpl(OutputStream outputStream) {
        super(outputStream);
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public void writeReport(Set<Calendar> dates, Set<User> users) {
        PrintStream ps = (outputStream.getClass() == PrintStream.class)
                ? (PrintStream) outputStream
                : new PrintStream(outputStream);
        for (Calendar date : dates) {
            String dateHeader =
                    date.get(Calendar.DATE)
                            + "-" + getMonth(date.get(Calendar.MONTH))
                            + "-" + Integer.toString(date.get(Calendar.YEAR));
            ps.println(dateHeader);
            ps.println();
            for (User user : users) {
                Map<Calendar, Set<VisitedURL>> dateVisitedURLMap = user.getVisitedURLByDate();
                if (dateVisitedURLMap.containsKey(date)) {
                    for (VisitedURL url : dateVisitedURLMap.get(date)) {
                        ps.append(user.getUserId()).append(',')
                                .append(url.getUrl()).append(',')
                                .append(Integer.toString(url.getAvgTime()))
                                .append("\n");
                    }
                }
            }
            ps.println();
        }
        ps.flush();
    }
}
