package net.barniley.domain.report;

import net.barniley.domain.User;

import java.io.OutputStream;
import java.util.Calendar;
import java.util.Set;

public abstract class Report {
    protected OutputStream outputStream;

    public Report(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public abstract void writeReport(Set<Calendar> dates, Set<User> users);
}
