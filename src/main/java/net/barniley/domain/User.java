package net.barniley.domain;

import net.barniley.domain.logfile.LogEntry;

import java.util.*;

public class User implements Comparable<User> {
    private String userId;
    private Map<Calendar, Set<VisitedURL>> visitedURLByDate;

    public User(String userId) {
        this.userId = userId;
        visitedURLByDate = new TreeMap<>();
    }

    public String getUserId() {
        return userId;
    }

    public Set<Calendar> getDate() {
        return visitedURLByDate.keySet();
    }

    public Map<Calendar, Set<VisitedURL>> getVisitedURLByDate() {
        return visitedURLByDate;
    }

    public boolean put(LogEntry logEntry) {
        if (!userId.equals(logEntry.getUserId())) {
            return false;
        }
        Set<VisitedURL> urls = visitedURLByDate.get(logEntry.getStartDate());
        if (urls == null) {
            visitedURLByDate.put(logEntry.getStartDate(), urls = new TreeSet<>());
        }
        VisitedURL visitedURL = new VisitedURL(logEntry.getUrl(), logEntry.getSpentTime());
        if (urls.contains(visitedURL)) {
            for (VisitedURL url : urls) {
                if (url.equals(visitedURL)) {
                    url.addTime(logEntry.getSpentTime());
                    break;
                }
            }
        } else {
            urls.add(visitedURL);
        }
        return true;
    }

    public void putAll(Collection<LogEntry> logEntries) {
        for (LogEntry logEntry : logEntries) {
            put(logEntry);
        }
    }


    @Override
    public int compareTo(User user) {
        return userId.compareTo(user.getUserId());
    }
}
