package net.barniley.domain.logfile;

import java.util.Calendar;

public class LogEntry {
    private Calendar startDate;
    private String userId;
    private String url;
    private int spentTimeInSeconds;

    public LogEntry(Calendar startDate, String userId, String url, int spentTime) {
        this.startDate = startDate;
        this.userId = userId;
        this.url = url;
        this.spentTimeInSeconds = spentTime;
    }

    public Calendar getStartDate() {
        return (Calendar) startDate.clone();
    }

    public String getUserId() {
        return userId;
    }

    public String getUrl() {
        return url;
    }

    public int getSpentTime() {
        return spentTimeInSeconds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogEntry logEntry = (LogEntry) o;

        if (spentTimeInSeconds != logEntry.spentTimeInSeconds) return false;
        if (!startDate.equals(logEntry.startDate)) return false;
        if (!userId.equals(logEntry.userId)) return false;
        return url.equals(logEntry.url);

    }

    @Override
    public int hashCode() {
        int result = startDate.hashCode();
        result = 31 * result + userId.hashCode();
        result = 31 * result + url.hashCode();
        result = 31 * result + spentTimeInSeconds;
        return result;
    }

    @Override
    public String toString() {
        return "LogEntry{" +
                "startDate=" + startDate +
                ", userId='" + userId + '\'' +
                ", url='" + url + '\'' +
                ", spentTimeInSeconds=" + spentTimeInSeconds +
                '}';
    }
}
