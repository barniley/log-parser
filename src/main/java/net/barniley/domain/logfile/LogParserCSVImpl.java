package net.barniley.domain.logfile;

import java.io.InputStream;
import java.util.Scanner;

public class LogParserCSVImpl extends LogFile {

    public LogParserCSVImpl(InputStream in) {
        super(in);
    }


    @Override
    public void parse() {
        Scanner scanner = new Scanner(inputStream);
        while (scanner.hasNextLine()) {
            String[] logLineValues = scanner.nextLine().split(",");
            long startTime = Long.parseLong(logLineValues[0]);
            String userId = logLineValues[1];
            String url = logLineValues[2];
            int spendTimeInSeconds = Integer.parseInt(logLineValues[3]);
            populate(startTime, userId, url, spendTimeInSeconds);
        }
    }

}
