package net.barniley.domain.logfile;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;

public class LogParserXMLImpl extends LogFile {

    public LogParserXMLImpl(InputStream inputStream) {
        super(inputStream);
    }

    @Override
    public void parse() {
        try {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            DefaultHandler handler = new DefaultHandler() {
                private boolean isUserId;
                private boolean isStartTime;
                private boolean isUrl;
                private boolean isSpentTime;

                private long startTime;
                private String userId;
                private String url;
                private int spentTime;

                @Override
                public void startElement(String s, String s1, String qName, Attributes attributes) throws SAXException {
                    switch (qName.toLowerCase()) {
                        case "userid":
                            isUserId = true;
                            break;
                        case "time":
                            isStartTime = true;
                            break;
                        case "url":
                            isUrl = true;
                            break;
                        case "spenttime":
                            isSpentTime = true;
                            break;
                    }
                }

                @Override
                public void endElement(String s, String s1, String qName) throws SAXException {
                    if (qName.equalsIgnoreCase("log")) {
                        populate(startTime, userId, url, spentTime);
                    }
                }

                @Override
                public void characters(char[] chars, int start, int length) throws SAXException {
                    if (isUserId) {
                        userId = new String(chars, start, length);
                        isUserId = false;
                    }
                    if (isStartTime) {
                        startTime = Long.parseLong(new String(chars, start, length));
                        isStartTime = false;
                    }
                    if (isUrl) {
                        url = new String(chars, start, length);
                        isUrl = false;
                    }
                    if (isSpentTime) {
                        spentTime = Integer.parseInt(new String(chars, start, length));
                        isSpentTime = false;
                    }
                }
            };
            parser.parse(inputStream, handler);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
