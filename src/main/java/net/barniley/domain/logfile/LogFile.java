package net.barniley.domain.logfile;

import net.barniley.domain.User;

import java.io.InputStream;
import java.util.*;

import static net.barniley.utils.Utils.*;

public abstract class LogFile {
    protected InputStream inputStream;
    private Set<LogEntry> logEntries;


    public LogFile(InputStream inputStream) {
        this.inputStream = inputStream;
        logEntries = new HashSet<>();
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public Set<LogEntry> getLogEntries() {
        return Collections.unmodifiableSet(logEntries);
    }

    protected void populate(long startTime, String userId, String url, int spentTimeInSeconds) {
        Calendar startDate = Calendar.getInstance();
        startDate.setTimeInMillis(startTime);

        Calendar endDate = Calendar.getInstance();
        endDate.setTimeInMillis(startTime + spentTimeInSeconds * MILLISECONDS_IN_SECOND);

        if (startDate.get(Calendar.DATE) == endDate.get(Calendar.DATE) &&
                startDate.get(Calendar.MONTH) == endDate.get(Calendar.MONTH) &&
                startDate.get(Calendar.YEAR) == endDate.get(Calendar.YEAR)) {
            add(new LogEntry(setDateToMidnight(startDate), userId, url, spentTimeInSeconds));
        } else {
            int startDateSpentTimeInSeconds =
                    (HOURS_IN_DAY - startDate.get(Calendar.HOUR_OF_DAY)) *
                            (MINUTES_IN_HOUR - startDate.get(Calendar.MINUTE)) *
                            (SECONDS_IN_MINUTE - startDate.get(Calendar.SECOND));
            int endDateSpentTimeInSeconds = spentTimeInSeconds - startDateSpentTimeInSeconds;
            add(new LogEntry(setDateToMidnight(startDate), userId, url, startDateSpentTimeInSeconds));
            add(new LogEntry(setDateToMidnight(endDate), userId, url, endDateSpentTimeInSeconds));
        }
    }

    protected boolean add(LogEntry logEntry) {
        return logEntries.add(logEntry);
    }

    public Set<String> getUserIds() {
        Set<String> userIds = new TreeSet<>();
        for (LogEntry logEntry : logEntries) {
            userIds.add(logEntry.getUserId());
        }
        return userIds;
    }

    public Set<Calendar> getDates() {
        Set<Calendar> dates = new TreeSet<>();
        for (LogEntry logEntry : this.logEntries) {
            dates.add(setDateToMidnight(logEntry.getStartDate()));
        }
        return dates;
    }

    public Set<LogEntry> getLogEntriesByUser(User user) {
        Set<LogEntry> logEntries = new HashSet<>();
        for (LogEntry logEntry : this.logEntries) {
            if (user.getUserId().equals(logEntry.getUserId())) {
                logEntries.add(logEntry);
            }
        }
        return logEntries;
    }

    public abstract void parse();
}
